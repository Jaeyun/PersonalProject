﻿using UnityEngine;
using System.Collections;

public class UISlideMenuCtrl : UILookParent {
    public GameObject objTargetPrefab;
    public GameObject [] objSubMenuList;
    public int m_Count;             //いくつ配置するか
    public bool isVertical;
    public Vector3 vecTrVal;
    public bool isOpenedSubmenu;
    public float fTimeMove = 0f;
    public float fDistictVal = 1.5f;
    public float fObjSize;
    public float fNoneLookTime = 0f;
	// Use this for initialization

    public void Init (int _Count = 0, bool _isVertical = false) 
    {
        m_Count = _Count;
        isVertical = _isVertical;
        Init();
    }
    public override void Init ()
    {
        base.Init();

        if (m_Count == 0)
        {
            if (objSubMenuList.Length == 0)
            {
                m_Count = 3;
            }
            else
            {
                m_Count = objSubMenuList.Length;
            }
        }

        if (objSubMenuList == null || objSubMenuList.Length == 0)
        {
            objSubMenuList = new GameObject[m_Count];
            for (int i = 0; i < objSubMenuList.Length; i++)
            {
                objSubMenuList[i] = Instantiate(objTargetPrefab);
                if (objSubMenuList[i].GetComponent<Renderer>() == null)
                {
                    objSubMenuList[i].AddComponent<Renderer>();
                }
                objSubMenuList[i].transform.parent = transform;
                objSubMenuList[i].transform.localPosition = Vector3.zero;
            }
        }
//        Debug.LogWarning("objSubMenuList.Length = " + objSubMenuList.Length + " , " + objSubMenuList[0].GetComponent<Renderer>());
        if (isVertical)
        {
            fObjSize = objSubMenuList[0].GetComponent<Renderer>().bounds.size.y;
        }
        else
        {
            fObjSize = objSubMenuList[0].GetComponent<Renderer>().bounds.size.x;
        }

        for (int i = 0; i < objSubMenuList.Length; i++)
        {
            objSubMenuList[i].SetActive(false);
        }
    }

    public override void LookingAction()
    {
        fTime += Time.deltaTime;
        if (fTime >= 1f)
        {
            UIAction();
            fTime = 0f;
            return;
        }
    }

    public override void NoneLookAction()
    {
        if (isOpenedSubmenu)
        {
            for (int i = 0; i < objSubMenuList.Length; i++)
            {
                if (objSubMenuList[i].GetComponent<Chk_isLookAtMe>().GetSet_isCamLookAtMe)
                {
                    fNoneLookTime = 0f;
                    return;
                }
            }
            fNoneLookTime += Time.deltaTime;
            Debug.LogError(gameObject.name + ", You Not Look Any SubList");
            if (fNoneLookTime > 3f)
            {
                Debug.LogError(gameObject.name + ", Close All SubList");
                UIAction();
                isOpenedSubmenu = false;
                fNoneLookTime = 0f;
            }
        }
//        for (int i = 0; i < objSubMenuList.Length; i++)
//        {
//            if (objSubMenuList[i].GetComponent<Chk_isLookAtMe>().GetSet_isCamLookAtMe)
//            {
//                Debug.LogWarning(gameObject.name + " is Look " + objSubMenuList[i].name);
//                if (!m_ChkIsLookAtMe.GetSet_isCamLookAtMe)
//                {
//                    m_ChkIsLookAtMe.GetSet_isCamLookAtMe = true;
//                }
//                SetTime(0f);
//                fNoneLookTime = 0f;
//                return;
//            }
//        }
//
//        if (isOpenedSubmenu)
//        {
//            fNoneLookTime += Time.deltaTime;
//            Debug.LogError(gameObject.name + " is Not Anything   ");
//            if (fNoneLookTime > 3f && isOpenedSubmenu)
//            {
//                Debug.LogError(gameObject.name + " is Not Anything, So Close All Submenu   ");
//                isOpenedSubmenu = false;
//                m_ChkIsLookAtMe.GetSet_isCamLookAtMe = false;
//                isLockCount = false;
//                fNoneLookTime = 0f;
//                SetSubMenuActive(false);
//            }
//        }
////        isLockChk = false;
////        fTime = 0f;
    }

    public override void UIAction() //UI実質的なアクション定義
    {
        StartCoroutine(UIOpenCloseProc());
    }

    IEnumerator UIOpenCloseProc()
    {
        Debug.LogWarning(gameObject.name + " : UIOpenCloseProc");
        fTimeMove = 0f;
        Vector3 tmpVec;

        while (fTimeMove < 1f)
        {
            fTimeMove += Time.deltaTime;
            yield return new WaitForFixedUpdate();
            for (int i = 0; i < objSubMenuList.Length; i++)
            {
                tmpVec = objSubMenuList[i].transform.localPosition;
                objSubMenuList[i].SetActive(true);
                if (!isOpenedSubmenu)
                {
                    if (isVertical)
                    {
                        tmpVec.y = Mathf.Lerp(0f, (i+1) * fDistictVal * fObjSize, fTimeMove);
                    }
                    else
                    {
                        tmpVec.x = Mathf.Lerp(0f, (i+1) * fDistictVal * fObjSize, fTimeMove);
                    }
                }
                else
                {
                    if (isVertical)
                    {
                        tmpVec.y = Mathf.Lerp(tmpVec.y, 0f, fTimeMove);
                    }
                    else
                    {
                        tmpVec.x = Mathf.Lerp(tmpVec.x, 0f, fTimeMove);
                    }
                }
                objSubMenuList[i].transform.localPosition = tmpVec;
            }
        }
        isOpenedSubmenu = !isOpenedSubmenu;
        for (int i = 0; i < objSubMenuList.Length; i++)
        {
            objSubMenuList[i].SetActive(isOpenedSubmenu);
        }
    }
    private void SetAllSubMenuActiveOff()
    {
        Debug.LogWarning(gameObject.name + " : SetSubMenuActive");
        for (int i = 0; i < objSubMenuList.Length; i++)
        {
            objSubMenuList[i].transform.localPosition = Vector3.zero;
            objSubMenuList[i].SetActive(false);
        }
        GetSet_fTime= 0f;
    }
}
