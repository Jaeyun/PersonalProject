﻿using UnityEngine;
using System.Collections;

public class UI_PivotTracker : MonoBehaviour {
    public Transform trTrackingTarget;
    public Transform trPlayer;
    private Vector3 rotateVec;
	// Use this for initialization
	void Start () {
        trTrackingTarget = GameObject.FindWithTag("UI_MenuPivot").transform;
        trPlayer = GameObject.FindWithTag("Player").transform;
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = trTrackingTarget.position;
        transform.forward = trPlayer.forward;
	}
}
