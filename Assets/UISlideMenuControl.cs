﻿using UnityEngine;
using System.Collections;

public class UISlideMenuControl : UILookParent {
    public GameObject objTargetPrefab;
    public GameObject [] objSubMenuList;
    public AudioSource m_SoundEff_Source;
    public AudioClip m_SoundEff_Clip;
    public int m_Count;             //いくつ配置するか
    public bool isLockTimeCount;
    public bool isOpenedMySubmenu;
    public bool isVertical;
    private Vector3 vecTrVal;
    private float fTimeMove = 0f;
    private float fDistictVal = 1.5f;
    private float fObjSize;




    public void Init (int _Count = 0, bool _isVertical = false) 
    {
        m_Count = _Count;
        isVertical = _isVertical;
        Init();
    }
    public override void Init ()
    {
        base.Init();
        SoundInfoData nowSoundData = new SoundInfoData();
        SoundCtrl soundCtrl = GameObject.FindWithTag("Manager").GetComponentInChildren<SoundCtrl>();
        GameObject tmpTargetObj = new GameObject();

        nowSoundData=soundCtrl.m_SoundInfosDic["ButtonSelect"];
        m_SoundEff_Clip = nowSoundData.GetSet_SoundClip;
        if (nowSoundData.GetSet_Target.Equals("Here"))
        {
            tmpTargetObj = gameObject;
        }
        else
        {
            tmpTargetObj = GameObject.FindWithTag(nowSoundData.GetSet_Target);
        }
        if (tmpTargetObj.GetComponent<AudioSource>() == null)
        {
            tmpTargetObj.AddComponent<AudioSource>();
        }
        m_SoundEff_Source = tmpTargetObj.GetComponent<AudioSource>();
        m_SoundEff_Source.clip = m_SoundEff_Clip;
        m_SoundEff_Source.loop = nowSoundData.GetSet_IsLoop;

        if (m_Count == 0)
        {
            if (objSubMenuList.Length == 0)
            {
                m_Count = 3;
            }
            else
            {
                m_Count = objSubMenuList.Length;
            }
        }

        if (objSubMenuList == null || objSubMenuList.Length == 0)
        {
            objSubMenuList = new GameObject[m_Count];
            for (int i = 0; i < objSubMenuList.Length; i++)
            {
                objSubMenuList[i] = Instantiate(objTargetPrefab);
                if (objSubMenuList[i].GetComponent<Renderer>() == null)
                {
                    objSubMenuList[i].AddComponent<Renderer>();
                }
                objSubMenuList[i].transform.parent = transform;
                objSubMenuList[i].transform.localPosition = Vector3.zero;
            }
        }
        if (isVertical)
        {
            fObjSize = objSubMenuList[0].GetComponent<Renderer>().bounds.size.y;
        }
        else
        {
            fObjSize = objSubMenuList[0].GetComponent<Renderer>().bounds.size.x;
        }

        for (int i = 0; i < objSubMenuList.Length; i++)
        {
            objSubMenuList[i].SetActive(false);
        }
    }

    public override void LookingAction()
    {
        fTime += Time.deltaTime;
        if (fTime > 1f && !isLockTimeCount)
        {
            UIAction();
            fTime = 0f;
            return;
        }
    }

    public override void UIAction() //UI実質的なアクション定義
    {
        if (!isOpenedMySubmenu)
        {
            m_SoundEff_Source.PlayOneShot(m_SoundEff_Clip);
            StartCoroutine(UIOpenProc());
            isOpenedMySubmenu = true;
        }
        else
        {
            SetAllSubMenuActiveOff();
            isOpenedMySubmenu = false;
        }
    }

    public void SetAllSubMenuActiveOff()
    {
        Debug.LogError(this.gameObject.name + "...Close All Submenu");
        for (int i = 0; i < objSubMenuList.Length; i++)
        {
            if (objSubMenuList[i].GetComponent<Chk_isLookAtMe>() != null)
            {
                objSubMenuList[i].GetComponent<Chk_isLookAtMe>().GetSet_isCamLookAtMe = false;
            }
            if (objSubMenuList[i].GetComponent<UISlideMenuControl>() != null)
            {
                objSubMenuList[i].GetComponent<UISlideMenuControl>().SetAllSubMenuActiveOff();
            }
            objSubMenuList[i].SetActive(false);
            objSubMenuList[i].transform.localPosition = Vector3.zero;
        }
        isOpenedMySubmenu = false;
        StartCoroutine(UiActionLock());
    }

    IEnumerator UIOpenProc()
    {
        isLockTimeCount = true;
        fTimeMove = 0f;
        Vector3 tmpVec;

        while (fTimeMove < 1f)
        {
            fTimeMove += Time.deltaTime;
            yield return new WaitForFixedUpdate();
            for (int i = 0; i < objSubMenuList.Length; i++)
            {
                tmpVec = objSubMenuList[i].transform.localPosition;
                objSubMenuList[i].SetActive(true);

                if (isVertical)
                {
                    tmpVec.y = Mathf.Lerp(0f, (i + 1) * fDistictVal * fObjSize, fTimeMove);
                }
                else
                {
                    tmpVec.x = Mathf.Lerp(0f, (i + 1) * fDistictVal * fObjSize, fTimeMove);
                }
                objSubMenuList[i].transform.localPosition = tmpVec;
            }
        }
        for (int i = 0; i < objSubMenuList.Length; i++)
        {
            objSubMenuList[i].SetActive(true);
        }
        fTimeMove = 0f;
        StartCoroutine(UiActionLock());
    }

    IEnumerator UiActionLock()
    {
        fTimeMove = 0f;
        while (fTimeMove < 1f)
        {
            fTimeMove += Time.deltaTime;
            yield return new WaitForFixedUpdate();
        }
        isLockTimeCount = false;
    }
}
