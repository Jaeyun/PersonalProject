﻿using UnityEngine;
using System.Collections;

public class MoveManager : MonoBehaviour {
	public Transform trNow;
	public Transform trTarget;
	GameObject objPlayer;
    GameObject [] objMovePtArr;
	float dist;

	void Start()
	{
        objMovePtArr = GameObject.FindGameObjectsWithTag("MovePoint");
		objPlayer = GameObject.FindWithTag("Player");
	}
	public void CtrlMove() {
		dist = Vector3.Distance(objPlayer.transform.position, trTarget.position);
		for (int i = 0; i < objMovePtArr.Length; i++) {
			if (!objMovePtArr [i].name.Equals (trTarget)) {
				objMovePtArr [i].GetComponent<Renderer> ().enabled = false;
				objMovePtArr [i].GetComponent<Collider> ().enabled = false;
			}
		}
        StartCoroutine(MoveProc());	
	}

	public bool SetBeforeTr(Transform _trNow){
		trNow = _trNow;
		return true;
	}
	public bool SetAfterTr(Transform _trTarget){
		trTarget = _trTarget;
		return true;
	}

	IEnumerator MoveProc(){
        Debug.LogWarning("Start dist : " + dist);
        while (dist > 0.01f) {
            objPlayer.transform.position = Vector3.Lerp(objPlayer.transform.position, trTarget.position, Time.deltaTime * 1.5f);
            yield return new WaitForEndOfFrame();
            dist = Vector3.Distance(objPlayer.transform.position, trTarget.position);
            //Debug.LogWarning("Move dist : " + dist);
		}
		for (int i = 0; i < objMovePtArr.Length; i++) {
			objMovePtArr [i].GetComponent<Renderer> ().enabled = true;
			objMovePtArr [i].GetComponent<Collider> ().enabled = true;
		}
        objPlayer.transform.position = trTarget.position;
        trNow = trTarget;
    }
}
