﻿using UnityEngine;
using System.Collections;

public class SystemCtrl : MonoBehaviour {
    public SysModeInfo m_SysModeInfo;
	// Use this for initialization
	void Awake () 
    {
        if (m_SysModeInfo == null)
        {
            m_SysModeInfo = SysModeInfo.Develop;
        }
	}

    public SysModeInfo GetSet_SysModeInfo
    {
        get { return m_SysModeInfo; }
        set { m_SysModeInfo = value; }
    }
}
