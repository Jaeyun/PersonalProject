﻿using UnityEngine;
using System.Collections;

public class Chk_isLookAtMe : MonoBehaviour {
    public bool isCamLookAtMe;
    public GameObject obj;
    void Start()
    {
        obj = this.gameObject;
    }
    public bool GetSet_isCamLookAtMe
    {
        get { return isCamLookAtMe; }
        set { isCamLookAtMe = value; }
    }
}
