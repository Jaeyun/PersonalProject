﻿using UnityEngine;
using System.Collections;

public class CamCtrl : MonoBehaviour
{
    private RaycastHit rayHit;
    private GameObject[] objArr;
    public Transform nowLook;
    public Transform nowLookBackUp;
    public GameObject objUITimeGuage;
    public Transform trPlayer;
    private Vector3 angleVec;
    int layerMask;
    // Use this for initialization
    void Start()
    {
        objUITimeGuage = GameObject.FindWithTag("UI_TimeGuage");
        objUITimeGuage.SetActive(false);
        layerMask = 1 << 11;                //ActiveObject 레이어를 통한 이중체크
        StartCoroutine(RetakeCamAndPlayerAngleGapProc());
    }

    IEnumerator RetakeCamAndPlayerAngleGapProc()
    {
        float fChkTime = 0f;
        while (true)
        {
            angleVec = Camera.main.transform.forward - trPlayer.forward;
            float dot = Vector3.Dot(Camera.main.transform.forward, trPlayer.forward);
            float angle = Mathf.Acos(dot) * Mathf.Rad2Deg;

//            Debug.LogWarning(angle);
            if (angle > 45f)
            {
                fChkTime += Time.deltaTime;
                if (fChkTime > 5f)
                {
                    Vector3 tmpVec = Camera.main.transform.forward;
                    trPlayer.forward = tmpVec;
                    Camera.main.transform.forward = tmpVec;
                    fChkTime = 0f;
                }
            }
            else
            {
                fChkTime = 0f;
            }
            yield return new WaitForFixedUpdate();
        }
    }

    // Update is called once per frame
    void LateUpdate()
    {
        Ray ray = new Ray();
        ray.origin = this.transform.position;         //레이를 쏠 원점 = 카메라.
        ray.direction = this.transform.forward;       //레이의 방향 = 카메라 방향
        Debug.DrawLine(ray.origin, ray.direction, Color.red, 10.0f);
        if (Physics.Raycast(ray, out rayHit, 30.0f, layerMask))
        {
            nowLook = rayHit.transform;
            //Debug.LogWarning(nowLook + " : " + nowLook.GetComponent<Chk_isLookAtMe>().isCamLookAtMe);
            if (nowLook.GetComponent<Chk_isLookAtMe>() == null)
            {
                nowLook.gameObject.AddComponent<Chk_isLookAtMe>();
            }
            if (!nowLook.GetComponent<Chk_isLookAtMe>().isCamLookAtMe)
            {
                if (nowLookBackUp != nowLook)
                {
                    if(nowLookBackUp != null)
                        nowLookBackUp.GetComponent<Chk_isLookAtMe>().isCamLookAtMe = false;
//                    Debug.LogWarning(nowLook + " / "+nowLookBackUp);
                    nowLookBackUp = nowLook;
                }
                nowLook.GetComponent<Chk_isLookAtMe>().isCamLookAtMe = true;
                if (!objUITimeGuage.activeSelf)
                {
                    objUITimeGuage.SetActive(true);
                }
            }
        }
        else
        {
            if (objUITimeGuage.activeSelf)
            {
                objUITimeGuage.SetActive(false);
            }
            if (nowLookBackUp != null)
            {
                nowLookBackUp.GetComponent<Chk_isLookAtMe>().isCamLookAtMe = false;
//                Debug.LogWarning(nowLook + " : " +nowLook.GetComponent<Chk_isLookAtMe>().isCamLookAtMe +
//                    "\n"+nowLookBackUp + " : " +nowLookBackUp.GetComponent<Chk_isLookAtMe>().isCamLookAtMe);
            }
            nowLook = nowLookBackUp = null;
        }
    }
}
