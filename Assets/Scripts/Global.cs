﻿using UnityEngine;
using System.Collections;

public enum SysModeInfo
{
    Develop = 1,    //開発用
    IternalTest,    //チーム内、少人数テスト(テストの便利のため、チーと使える)
    OutsideTest,    //ステージング、審査、外部向けテスト(ユーザーと同じ環境)
    Release,        //リリース
}

public enum TimeInfo : int
{
    CountDown = 5,
    UILook = 1,
}


public enum SceneInfo : int
{
    Logo = 0,
    Main,
}

public enum SoundType : int
{
    System = 1,
    Effect,
    BGM,
    Voice,
}

public enum SoundInfoStruct : int
{
    key = 1,
    Path = 2,
    FileName = 3,
    SoundType = 4,
    Loop = 5,
    Target = 6,
}