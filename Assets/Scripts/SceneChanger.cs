﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SceneChanger : MonoBehaviour {
    public SceneInfo m_sceneInfo;
    public GameObject m_objCharacter;
	// Use this for initialization
	private void Start () 
    {
        m_objCharacter = GameObject.FindWithTag("Player");
        m_sceneInfo = SceneInfo.Logo;
	}

    public void SceneChange(SceneInfo _targetSceneInfo)
    {
        StartCoroutine(SceneChangeProc(_targetSceneInfo));
    }
    IEnumerator SceneChangeProc(SceneInfo _targetSceneInfo)
    {
        Transform tr;
        SceneManager.LoadScene((int)_targetSceneInfo);
        bool isActive = true;
        while (isActive)
        {
            if (Application.loadedLevel == (int)_targetSceneInfo)
            {
                //GameObject.FindWithTag("SubCamera").SetActive(false);
                yield return new WaitForFixedUpdate();
                m_sceneInfo = _targetSceneInfo;
                tr = GameObject.FindWithTag("StartPoint").transform;
                m_objCharacter.transform.position = tr.position;
                m_objCharacter.transform.rotation = tr.rotation;
                yield return new WaitForFixedUpdate();
                isActive = false;
                break;
            }
            yield return new WaitForFixedUpdate();
        }
    }
}
