﻿using UnityEngine;
using System.Collections;

public class UILookParent : MonoBehaviour {
    public Chk_isLookAtMe m_ChkIsLookAtMe;
    public float fTime;
    private Vector3 m_vecOrigin;
    void Start()
    {
        Init();
    }
	// Update is called once per frame
	protected virtual void FixedUpdate () 
    {
//        Debug.LogWarning(gameObject.name + " : FixedUpdate");
        if (m_ChkIsLookAtMe == null)
        {
            Init();
            return;
        }
        if (!m_ChkIsLookAtMe.isCamLookAtMe)
        {
            NoneLookAction();
            return;
        }
        else if(m_ChkIsLookAtMe.isCamLookAtMe)
        {
//            Debug.LogWarning(gameObject.name + " : LookingAction");
            LookingAction();
        }
	}

    public virtual void Init()
    {
        //Debug.LogWarning(gameObject.name + " : Base.Init");
        m_vecOrigin = transform.localScale;
        fTime = 0f;
        m_ChkIsLookAtMe = GetComponent<Chk_isLookAtMe>();
        if (m_ChkIsLookAtMe == null)
        {
            gameObject.AddComponent<Chk_isLookAtMe>();
        }
    }
    public virtual void NoneLookAction()
    {
        GetComponent<Transform>().localScale = m_vecOrigin; 
        fTime = 0f;
    }
    public virtual void LookingAction()
    {
        if (fTime < 1f)
        {
            transform.localScale = m_vecOrigin * (1f + fTime);
        }
        else
        {
            UIAction();
            m_ChkIsLookAtMe.isCamLookAtMe = false;
            fTime = 0f;
            return;
        }
    }
    public virtual void UIAction()
    {
        transform.localScale = m_vecOrigin;
    }
    public float GetSet_fTime
    {
        get{ return fTime; }
        set{ fTime = value; }
         }
}
