﻿using UnityEngine;
using System.Collections;

public class OvrManager : MonoBehaviour 
{
    public GvrViewer m_objGvr;
    public GameObject m_normalUI;
    public GameObject m_objPlayer;
    public DeviceOrientation m_deviceOrientation;
    public bool isVrMode;
    public GameObject tmpLogo;
    public UILabel tmpUILabel;
    public SystemCtrl m_systemCtrl;

   

    private SystemCtrl m_objSystemCtrl;
	// Use this for initialization
    void Awake()
    {
        if (m_objGvr == null)
        {
            m_objGvr = FindObjectOfType<GvrViewer>();
        }
        if (m_normalUI == null)
        {
            m_normalUI = new GameObject();
        }
        m_deviceOrientation = DeviceOrientation.Portrait;
        m_systemCtrl = GetComponent<SystemCtrl>();
        DontDestroyOnLoad(this);
        DontDestroyOnLoad(m_objPlayer);
        m_normalUI.SetActive(!m_objGvr.VRModeEnabled );
        SwapUIMode(false);
    }
	void Start () 
    {
        //tmpUILabel.text = m_deviceOrientation.ToString();
        CahngeUIMode(SceneInfo.Main);
	}
	
	// Update is called once per frame
	void Update () 
    {
        /*
        m_deviceOrientation = Input.deviceOrientation;
        if (m_deviceOrientation == DeviceOrientation.Portrait)
        {
            SwapUIMode(false);
            return;
        }
        else if (m_deviceOrientation == DeviceOrientation.LandscapeLeft || m_deviceOrientation == DeviceOrientation.LandscapeRight)
        {
            SwapUIMode(true);
            return;
        }
        */
    }
    public void CahngeUIMode(SceneInfo _targetSceneInfo)
    {
        StartCoroutine(ChangeVrModeProc(_targetSceneInfo));
    }

    public void CahngeUIMode()
    {
        StartCoroutine(ChangeVrModeProc());
    }

    IEnumerator ChangeVrModeProc()
    {
        int nCountTime = 0;
        bool isAbleActive = true;
        while(isAbleActive)
        {
            if (m_systemCtrl.GetSet_SysModeInfo == SysModeInfo.Develop)
            {
                //tmpUILabel.text = tmpUILabel.text = m_deviceOrientation.ToString() + "   " + isAbleActive + "   " + nCountTime.ToString();
            }
            else
            {
                m_deviceOrientation = Input.deviceOrientation;
                //tmpUILabel.gameObject.SetActive(false);
            }
            //VRModeへの変更場合は真っ白なスプライトを表示、５秒間カウントダウンをしてからモード変換
            if ((m_deviceOrientation == DeviceOrientation.LandscapeLeft || m_deviceOrientation == DeviceOrientation.LandscapeRight) && !isVrMode)
            {
                nCountTime++;
                m_normalUI.GetComponentInChildren<UITexture>().color = new Color(1,1,1,((int)TimeInfo.CountDown - nCountTime) / (int)TimeInfo.CountDown);
                if (nCountTime >= (int)TimeInfo.CountDown)
                {
//                    m_normalUI.GetComponentInChildren<UITexture>().alpha = 0f;
                    isVrMode = true;
                    nCountTime = 0;
                    isAbleActive = false;
                }
            }
            else if ((m_deviceOrientation == DeviceOrientation.Portrait || m_deviceOrientation == DeviceOrientation.PortraitUpsideDown) && isVrMode)
            {
                nCountTime++;
                m_normalUI.GetComponentInChildren<UITexture>().color = new Color(1,1,1,nCountTime / (int)TimeInfo.CountDown);
                if (nCountTime >= (int)TimeInfo.CountDown)
                {
//                    m_normalUI.GetComponentInChildren<UITexture>().alpha = 1f;
                    isVrMode = false;
                    nCountTime = 0;
                    isAbleActive = false;
                }
            }
            yield return new WaitForSeconds(1);
        }
        SwapUIMode(isVrMode);
    }
    IEnumerator ChangeVrModeProc(SceneInfo _targetSceneInfo)
    {
        int nCountTime = 0;
        bool isAbleActive = true;
        while(isAbleActive)
        {
            if (m_systemCtrl.GetSet_SysModeInfo != SysModeInfo.Develop)
            {
                m_deviceOrientation = Input.deviceOrientation;
            }
            //tmpUILabel.text = tmpUILabel.text = m_deviceOrientation.ToString() + "   "+ isAbleActive + "   " +nCountTime.ToString();
            ////VRModeへの変更場合は真っ白なスプライトを表示、５秒間カウントダウンをしてからモード変換
            if ((m_deviceOrientation == DeviceOrientation.LandscapeLeft || m_deviceOrientation == DeviceOrientation.LandscapeRight) && !isVrMode)
            {
                nCountTime++;
                //m_normalUI.GetComponentInChildren<UITexture>().color = new Color(1,1,1,((int)TimeInfo.CountDown - nCountTime) / (int)TimeInfo.CountDown);
                if (nCountTime >= (int)TimeInfo.CountDown)
                {
                    //                    m_normalUI.GetComponentInChildren<UITexture>().alpha = 0f;
                    isVrMode = true;
                    nCountTime = 0;
                    isAbleActive = false;
                }
            }
            else if ((m_deviceOrientation == DeviceOrientation.Portrait || m_deviceOrientation == DeviceOrientation.PortraitUpsideDown) && isVrMode)
            {
                nCountTime++;
                m_normalUI.GetComponentInChildren<UITexture>().color = new Color(1,1,1,nCountTime / (int)TimeInfo.CountDown);
                if (nCountTime >= (int)TimeInfo.CountDown)
                {
                    //                    m_normalUI.GetComponentInChildren<UITexture>().alpha = 1f;
                    isVrMode = false;
                    nCountTime = 0;
                    isAbleActive = false;
                }
            }
            yield return new WaitForSeconds(1);
        }
        SwapUIMode(isVrMode);
        GetComponent<SceneChanger>().SceneChange(_targetSceneInfo);
    }

    private void SwapUIMode()
    {
        m_objGvr.enabled = !m_objGvr.enabled;
        m_objGvr.VRModeEnabled = m_objGvr.enabled;
        m_normalUI.SetActive(!m_objGvr.VRModeEnabled );
    }
    private void SwapUIMode(bool isVR)
    {
        m_objGvr.enabled = isVR ;
        m_objGvr.VRModeEnabled = m_objGvr.enabled;
        m_normalUI.SetActive(!isVR );
    }
}