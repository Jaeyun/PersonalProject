﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class SoundInfoData
{
    private AudioClip m_Clip;
    private SoundType m_Type;
    private string m_Key;
    private bool isLoop;
    private string m_Target;

    public AudioClip GetSet_SoundClip
    {
        get { return m_Clip; }
        set { m_Clip = value; }
    }

    public SoundType GetSet_SoundType
    {
        get { return m_Type; }
        set { m_Type = value;}
    }

    public string GetSet_Key
    {
        get { return m_Key; }
        set { m_Key = value; }
    }

    public bool GetSet_IsLoop
    {
        get { return isLoop; }
        set { isLoop = value; }
    }

    public string GetSet_Target
    {
        get { return m_Target; }
        set { m_Target = value; }
    }
}


public class SoundCtrl : MonoBehaviour {
    //public SoundInfo [] m_SoundInfos;
    public TextAsset m_SoundInfoTable;
    public Dictionary<string, SoundInfoData> m_SoundInfosDic;


    private void Start()
    {
        m_SoundInfosDic = new Dictionary<string, SoundInfoData>();
        StringReader reader = new StringReader(m_SoundInfoTable.text);
        int rowCount = 0;
        while (reader.Peek() > -1) 
        {
            string line = reader.ReadLine();
            Debug.LogError(line);
            string [] values = line.Split(',');
            if (string.IsNullOrEmpty(values[(int)SoundInfoStruct.key]))
            {
                break;
            }
            if (rowCount == 0)
            {
                rowCount++;
                continue;
            }
            string path = "";
            SoundInfoData  m_SoundInfoData = new SoundInfoData();

            for (int i = 0; i < values.Length; i++)
            {
                switch (i)
                {
                    case (int)SoundInfoStruct.key:
                        m_SoundInfoData.GetSet_Key = values[i];
                        break;

                    case (int)SoundInfoStruct.Path:
                        path = values[i];
                        break;

                    case (int)SoundInfoStruct.FileName:
                        path += values[i];
                        m_SoundInfoData.GetSet_SoundClip = Resources.Load(path) as AudioClip;
                        break;

                    case (int)SoundInfoStruct.Loop:
                        if (values[i].Equals("true", System.StringComparison.OrdinalIgnoreCase)
                            || values[i].Contains("1"))
                        {
                            m_SoundInfoData.GetSet_IsLoop = true;
                        }
                        break;
                        
                    case (int)SoundInfoStruct.Target:
                        m_SoundInfoData.GetSet_Target = values[i];
                        break;

                    case (int) SoundInfoStruct.SoundType:
                        if (values[i].Contains("System"))
                            m_SoundInfoData.GetSet_SoundType = SoundType.System;
                        else if (values[i].Contains("Effect"))
                            m_SoundInfoData.GetSet_SoundType = SoundType.Effect;
                        else if (values[i].Contains("BGM"))
                            m_SoundInfoData.GetSet_SoundType = SoundType.BGM;
                        else if (values[i].Contains("Voice"))
                            m_SoundInfoData.GetSet_SoundType = SoundType.Voice;
                        break;
                }
            }
            Debug.LogError(m_SoundInfoData.GetSet_Key + ", " + m_SoundInfoData.GetSet_SoundClip);
            m_SoundInfosDic.Add(m_SoundInfoData.GetSet_Key, m_SoundInfoData);

            rowCount++;
        }
    }
}
