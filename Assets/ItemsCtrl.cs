﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ItemsCtrl : MonoBehaviour 
{
    public TextAsset [] m_TextTable;
    public Dictionary<string, TextAsset> m_DicTextTable;

    void Awake()
    {
        m_DicTextTable = new Dictionary<string, TextAsset>();
        for (int i = 0; i < m_TextTable.Length; i++)
        {
            m_DicTextTable.Add(m_TextTable[i].name, m_TextTable[i]);
            Debug.LogWarning(m_TextTable[i].name);
        }
    }

    public void GetTextData(string _key, out TextAsset _targetVal)
    {
        if (m_DicTextTable[_key] != null)
        {
            _targetVal = m_DicTextTable[_key];
        }
        else
        {
            Debug.LogError("[GetTextData] \"" + _key + "\" was Not Exist.");
            _targetVal = new TextAsset();
        }
    }
}
