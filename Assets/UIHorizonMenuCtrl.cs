﻿using UnityEngine;
using System.Collections;

public class UIHorizonMenuCtrl : UILookParent {
    public GameObject [] objArr_UI_Sub;
    public Vector3 vecTrVal;
    public GameObject objTargetPrefab;
    public bool isOpened;
    float fTimeMove = 0f;
    float fDistictVal = 1.5f;
    float fWidth;
    /*
    public override void Init() //本格的なInit作業
    {
        base.Init();
        fWidth = objTargetPrefa.GetComponent<Renderer>().bounds.size.x;
        vecTrVal = new Vector3(1f, 0f, 0f);
        for (int i = 0; i < objArr_UI_Sub.Length; i++)
        {
            objArr_UI_Sub[i].SetActive(isOpened);
        }
    }

    public override void UIAction() //UI実質的なアクション定
    {
        base.UIAction();
        StartCoroutine(UIOpenCloseProc());
    }

    public override void LookingAction()
    {
        SetTime(GetTime()+Time.deltaTime);
        if (GetTime() > 1f && !GetIsLockChk())
        {
            Debug.LogWarning(gameObject.name + " : LookingAction");
            UIAction();
            SetIsLockChk(true);
        }
        else
        {
        }
    }

    IEnumerator UIOpenCloseProc()
    {
        Debug.LogWarning(gameObject.name + " : UIOpenCloseProc");
        fTimeMove = 0f;
        Vector3 tmpVec;

        while (fTimeMove < 1f)
        {
            fTimeMove += Time.deltaTime;
            yield return new WaitForFixedUpdate();
            for (int i = 0; i < objArr_UI_Sub.Length; i++)
            {
                tmpVec = objArr_UI_Sub[i].transform.localPosition;
                Debug.LogWarning(objArr_UI_Sub[i].GetComponent<Renderer>().bounds.size);
                objArr_UI_Sub[i].SetActive(true);
                if (!isOpened)
                {
                    tmpVec.x = Mathf.Lerp(0f, (i+1) * fDistictVal * objArr_UI_Sub[i].GetComponent<Renderer>().bounds.size.x, fTimeMove);
                }
                else
                {
                    tmpVec.x = Mathf.Lerp(tmpVec.x, 0f, fTimeMove);
                }
                objArr_UI_Sub[i].transform.localPosition = tmpVec;
            }
        }
        isOpened = !isOpened;
        for (int i = 0; i < objArr_UI_Sub.Length; i++)
        {
            objArr_UI_Sub[i].SetActive(isOpened);
        }
    }
    */
}
